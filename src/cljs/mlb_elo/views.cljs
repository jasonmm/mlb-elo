(ns mlb-elo.views
  (:require
   [re-frame.core :as re-frame]
   [goog.string :as gstr]
   [goog.string.format]
   [mlb-elo.subs :as subs]
   [mlb-elo.events :as events]
   [clojure.string :as str]))



;; home

(defn loading
  []
  (let [loading? @(re-frame/subscribe [::subs/loading?])]
    [:span
     {:style (if loading?
               {:visibility "visible"}
               {:visibility "hidden"})}
     " Loading..."]))

(defn get-standings-error
  []
  (let [err-msg @(re-frame/subscribe [::subs/error-msg])]
    (:span
     {:style (if (str/blank? err-msg)
               {:visibility "hidden"}
               {:visibility "visible"})}
     [:b err-msg])))

(defn standings
  []
  (let [standings @(re-frame/subscribe [::subs/get-standings])]
    (if (not-empty standings)
      [:div
       [:button {:type "button"
                 :on-click #(re-frame/dispatch [::events/first-standings])} "<< First"]

       [:button {:type "button"
                 :on-click #(re-frame/dispatch [::events/prev-standings])} "< Previous"]

       [:span " | "]

       [:button {:type "button"
                 :on-click #(re-frame/dispatch [::events/middle-standings])} "Middle"]

       [:span " | "]

       [:button {:type "button"
                 :on-click #(re-frame/dispatch [::events/next-standings])} "Next >"]

       [:button {:type "button"
                 :on-click #(re-frame/dispatch [::events/last-standings])} "Last >>"]

       [:table
        {:width "25%"}
        [:thead
         [:tr
          [:th "Team"]
          [:th "Elo"]
          [:th "W"]
          [:th "L"]]]
        [:tbody
         (for [team standings]
           [:tr
            [:td (first team)]
            [:td {:style {:text-align "center"}} (gstr/format "%.1f" (:rating (second team)))]
            [:td {:style {:text-align "center"}} (:w (second team))]
            [:td {:style {:text-align "center"}} (:l (second team))]])]]])))

(defn year-kfactor-form
  []
  [:table
   [:tbody
    [:tr
     [:td
      [:label {:for "year"} "Year: "]
      [:input {:type "text"
               :size 4
               :default-value "1956"
               :value @(re-frame/subscribe [::subs/get-year])
               :name "year"
               :id "year"
               :on-change #(re-frame/dispatch [::events/set-year (-> % .-target .-value)])}]]
     [:td
      [:label {:for "k-factor"} "K-Factor: "]
      [:input {:type "text"
               :size 2
               :default-value "1"
               :value @(re-frame/subscribe [::subs/get-kfactor])
               :name "k-factor"
               :id "k-factor"
               :on-change #(re-frame/dispatch [::events/set-kfactor (-> % .-target .-value)])}]]
     [:td
      [:button {:type "button"
                :on-click #(re-frame/dispatch [::events/get-elo-standings])} "Submit"]]
     [:td
      [loading]
      [get-standings-error]]]]])

(defn home-panel
  []
  [:div
   [:h3 "MLB Elo Standings"]
   [year-kfactor-form]

   [:hr]

   [standings]])




;; about

(defn about-panel
[]
[:div
 [:h1 "This is the About Page."]

 [:div
  [:a {:href "#/"}
   "go to Home Page"]]])


;; main

(defn- panels
[panel-name]
(case panel-name
  :home-panel [home-panel]
  :about-panel [about-panel]
  [:div]))

(defn show-panel
[panel-name]
[panels panel-name])

(defn main-panel
[]
(let [active-panel (re-frame/subscribe [::subs/active-panel])]
  [show-panel @active-panel]))
