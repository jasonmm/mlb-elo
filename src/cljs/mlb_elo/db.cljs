(ns mlb-elo.db)

(def default-db
  {:year            "1956"
   :k-factor        "1"
   :get-standings   {:loading?  false
                     :error-msg ""}
   :standings-index 0
   :standings       []})
