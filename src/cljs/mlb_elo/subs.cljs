(ns mlb-elo.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))

(re-frame/reg-sub
 ::loading?
 (fn [db _]
   (get-in db [:get-standings :loading?])))

(re-frame/reg-sub
 ::error-msg
 (fn [db _]
   (get-in db [:get-standings :error-msg])))

(re-frame/reg-sub
 ::get-standings
 (fn [db _]
   (if (not-empty (:standings db))
     (nth (:standings db) (:standings-index db))
     [])))

(re-frame/reg-sub
 ::get-year
 (fn [db _]
   (:year db)))

(re-frame/reg-sub
 ::get-kfactor
 (fn [db _]
   (:k-factor db)))

(re-frame/reg-sub
 ::get-standings-index
 (fn [db _]
   (:standings-index db)))
