(ns mlb-elo.events
  (:require
   [re-frame.core :as re-frame]
   [mlb-elo.db :as db]
   [mlb-elo.config :as config]
   [ajax.core :as ajax]
   [day8.re-frame.http-fx]
   [day8.re-frame.tracing :refer-macros [fn-traced defn-traced]]))

(re-frame/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-panel
 (fn-traced [db [_ active-panel]]
            (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
 ::set-year
 (fn [db [_ year]]
   (assoc db :year year)))

(re-frame/reg-event-db
 ::set-kfactor
 (fn [db [_ k]]
   (.log js/console k)
   (assoc db :k-factor k)))

(re-frame/reg-event-db
 ::first-standings
 (fn [db _]
   (assoc db :standings-index 0)))

(re-frame/reg-event-db
 ::middle-standings
 (fn [db _]
   (assoc db :standings-index (quot (count (:standings db)) 2))))

(re-frame/reg-event-db
 ::last-standings
 (fn [db _]
   (assoc db :standings-index (- (count (:standings db)) 1))))

(re-frame/reg-event-db
 ::standings-success
 (fn [db [_ response]]
   (-> db
       (assoc-in [:get-standings :loading?] false)
       (assoc :standings-index 0)
       (assoc :standings (js->clj response)))))

(re-frame/reg-event-db
 ::next-standings
 (fn [db _]
   (update db :standings-index #(+ % 15))))

(re-frame/reg-event-db
 ::prev-standings
 (fn [db _]
   (update db :standings-index #(- % 15))))

(re-frame/reg-event-db
 ::standings-failure
 (fn [db [_ response]]
   (-> db
       (assoc-in [:get-standings :error-msg] "Error retrieving Elo standings for that year. Make sure you entered a valid MLB year.")
       (assoc-in [:get-standings :loading?] false))))

(re-frame/reg-event-fx
 ::get-elo-standings
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :get
                 :uri             (str config/standings-url (:year db) "/" (:k-factor db))
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [::standings-success]
                 :on-error        [::standings-failure]}
    :db         (assoc-in db [:get-standings :loading?] true)}))
