(ns mlb-elo.core
  (:gen-class)
  (:require [ring.adapter.jetty :refer :all]
            [ring.middleware.content-type :refer :all]
            [ring.middleware.resource :refer :all]
            [ring.middleware.not-modified :refer :all]
            [environ.core :refer [env]]
            [clojure.java.io :as io]
            [compojure.api.sweet :refer :all]
            [mlb-elo.elo :as elo]))

(defn get-standings
  [year k-factor]
  (elo/elo-from-year year (or (Integer/parseInt k-factor) elo/default-k-factor)))

(defapi mlb-elo-api
  (GET "/" [] (io/resource "public/index.html"))
  (GET "/standings/:year/:k" [year k] (get-standings year k)))

(def app
  (-> mlb-elo-api
      (wrap-resource "public")
      (wrap-content-type)
      (wrap-not-modified)))

(defn -main
  [& [port]]
  (let [port (Integer. (or port (env :port) 3000))]
    (run-jetty #'app {:port port :join? false})))
