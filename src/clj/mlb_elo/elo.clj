(ns mlb-elo.elo
  (:gen-class)
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.math.numeric-tower :as math]
            [clj-http.client :as client]
            [fipp.clojure :as fipp]))

;; The K-factor used in the Elo calculation. For this application 1 seems to
;; be the appropriate value.
(def default-k-factor 1)

(defn expected-score
  "Calculates the given teams' expected score based on their current ratings
  in `elo-ratings`. Remember 'score' in this context is 1 for a win and
  0 for a loss."
  [team-a team-b elo-ratings]
  (let [rating-a    (get-in elo-ratings [team-a :rating])
        rating-b    (get-in elo-ratings [team-b :rating])
        qa          (math/expt 10 (/ rating-a 400))
        qb          (math/expt 10 (/ rating-b 400))
        denominator (+ qa qb)]
    {:expected-a (/ qa denominator)
     :expected-b (/ qb denominator)}))

(defn elo
  "Calculates the teams' new ratings given the passed in scores and their
  current ratings in `elo-ratings`. Remmber 'score' in this context
  is 1 for a win and 0 for a loss."
  [team-a team-b team-a-score team-b-score elo-ratings k-factor]
  (let [rating-a        (get-in elo-ratings [team-a :rating])
        rating-b        (get-in elo-ratings [team-b :rating])
        expected-scores (expected-score team-a team-b elo-ratings)]
    {:rating-a (+ rating-a (* k-factor (- team-a-score (:expected-a expected-scores))))
     :rating-b (+ rating-b (* k-factor (- team-b-score (:expected-b expected-scores))))}))

(defn ensure-default-rating
  "If `team` is not a key in `elo-rating` then add it as a key with a rating
  of 1200."
  [team elo-ratings]
  (if (contains? elo-ratings team)
    elo-ratings
    (assoc elo-ratings team {:rating 1200
                             :w      0
                             :l      0})))

(defn w-or-l
  "Returns a `:w` if `score` is 1, otherwise returns a `:l`."
  [score]
  (if (= 1 score) :w :l))

(defn elo-ratings
  "Returns a reducing function used to calculate the new Elo rating and W/L
  record for the teams in `row`. `row` is a parsed CSV line from a Retrosheet
  game log file."
  [k-factor]
  (fn [carry row]
    (let [visitor                     (nth row 3)
          home                        (nth row 6)
          visitor-runs                (Integer/parseInt (nth row 9))
          home-runs                   (Integer/parseInt (nth row 10))
          [visitor-score home-score]  (if (> visitor-runs home-runs) [1 0] [0 1])
          carry                       (ensure-default-rating visitor carry)
          carry                       (ensure-default-rating home carry)
          {:keys [rating-a rating-b]} (elo visitor home visitor-score home-score carry k-factor)
          home-data                   (-> (get carry home)
                                          (update (w-or-l home-score) + 1)
                                          (assoc :rating rating-b))
          visitor-data                (-> (get carry visitor)
                                          (update (w-or-l visitor-score) + 1)
                                          (assoc :rating rating-a))]
      (assoc carry visitor visitor-data home home-data))))

(defn games-with
  "Returns a function that returns true if the string `team` is in the 3rd or
  6th cell of the seq `row`."
  [team]
  (fn [row]
   (or (= team (nth row 3)) (= team (nth row 6)))))

(defn sort-teams
  "Sorts the seq of `teams` by their rating from highest to lowest."
  [teams]
  (reverse (sort-by #(->> % second :rating) teams)))

(defn filter-incomplete-team-set
  "Filters groups of teams that do not include the `complete-cnt` number of
  teams. At the beginning not all teams have played a game so the sequences
  of teams is incomplete. Seasons can have a different number of total teams
  so `complete-cnt` is passed in as the expected total number of teams in the
  league."
  [elo-ratings complete-cnt]
  (filter #(<= complete-cnt (count %)) elo-ratings))

(defn calc-elo-standings
  "Given a `filename` of a Retrosheet season game log file, return a sequence
  of team/data pairs for each team after every game. The 'data' part is a map
  of the team's Elo rating and W/L record."
  [games k-factor]
  (->> games
       (reductions (elo-ratings k-factor) {})
       (map #(into [] %))
       (into [])
       (drop 1)  ;; for some reason the first element is a blank vector
       (map sort-teams)))

(defn download
  "Return the `:body` from `url` as a byte array. The byte array part is
  necessary because we expect `url` to be a zip file."
  [url]
  (-> (client/get url {:as :byte-array}) :body))

(defn unzip
  "Convert the byte-array `s` into an input stream that will unzip the string.
  From https://stackoverflow.com/a/32745253"
  [s]
  (let [stream (-> s
                   io/input-stream
                   (java.util.zip.ZipInputStream.))]
    (.getNextEntry stream)
    stream))

(defn elo-from-filename
  "Given a filename of an unzipped Retrosheet game log file return the Elo
  standings for that season."
  [filename]
  (with-open [reader (io/reader filename)]
    (let [games (csv/read-csv reader)]
      (calc-elo-standings games))))

(defn elo-from-year
  "Given a `year`, this function downloads the Retrosheet game log file
  for that season and returns the Elo standings."
  ([year]
   (elo-from-year year default-k-factor))
  ([year k-factor]
   (let [url (str "http://www.retrosheet.org/gamelogs/gl" year ".zip")]
     (with-open [reader (-> url download unzip io/reader)]
       (let [games (csv/read-csv reader)]
         (calc-elo-standings games k-factor))))))

(defn -main
  [& args]
  (let [filename (first args)
        all-elo  (calc-elo-standings filename)]
    (fipp/pprint (sort-by #(->> % second :rating) (last all-elo)))))

(defn sorted-last
  "Takes the sequence of Elo ratings for the entire season, grabs the final
  one, sorts it by rating and returns it."
  [elo-ratings]
  (->> elo-ratings
       last
       sort-teams))

(defn csv-ize
  [elo-ratings]
  (loop [csv         [["Team" "Rating" "Wins" "Losses"]]
         elo-ratings elo-ratings]
    (let [data                 (peek elo-ratings)
          team                 (first data)
          {:keys [rating w l]} (second data)
          next-elo-ratings     (rest elo-ratings)
          csv                  (conj csv [team rating w l])]
      (if (not (empty? next-elo-ratings))
        (recur csv next-elo-ratings)
        csv))))

(defn csv-elo
  [input-filename output-filename]
  (with-open [writer (io/writer output-filename)]
    (->> input-filename
         calc-elo-standings
         sorted-last
         csv-ize
         (csv/write-csv writer))))

(defn print-elo
  "Call this fn from the REPL to print the final ratings in order."
  [filename]
  (fipp/pprint (-> filename
                   elo-from-filename
                   last
                   sort-teams)))
